let bmwandAudi = (inventory, maker1, maker2) => {
  if (Array.isArray(inventory)) {
    const bmwandaudi = inventory.filter((car) => {
      return car.car_make === maker1 || car.car_make === maker2;
    });
    return bmwandaudi;
  }
  return null;
};

module.exports = bmwandAudi;
