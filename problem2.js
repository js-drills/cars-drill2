const lastCarInventory = (inventory) => {
  if (Array.isArray(inventory)) {
    let lastCar = inventory.filter((car, index) => {
      return index === inventory.length - 1;
    });
    return lastCar;
  }
  return null;
};

module.exports = lastCarInventory;
