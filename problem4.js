let carYears = (inventory) => {
  if (Array.isArray(inventory)) {
    const years = inventory.map((car) => {
      return car.car_year;
    });
    return years;
  }
  return null;
};

module.exports = carYears;
