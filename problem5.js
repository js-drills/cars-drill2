let olderthan200 = (carYears) => {
  if (Array.isArray(carYears)) {
    const filteredyears = carYears.filter((years) => {
      return years < 2000;
    });
    return filteredyears;
  }
  return null;
};

module.exports = olderthan200;
