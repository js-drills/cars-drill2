
let alphabeticalOrderSort = (inventory) => {
    if (Array.isArray(inventory)) {
        let sortedArray = inventory.sort((firstcar, secondCar) => {
            return firstcar.car_model.localeCompare(secondCar.car_model)
        })
        return sortedArray;
    }
    return null;
}

module.exports = alphabeticalOrderSort