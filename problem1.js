const inventory = require("../Drill1/ineventoy");

let carusingId = (inventory, carId) => {
  if (Array.isArray(inventory)) {
    let cardata = inventory
      .filter((car) => {
        return car.id === carId;
      })
      .reduce((acc, car) => {
        return (
          acc +
          `Car ${carId} is a ${car.car_year}  ${car.car_make} ${car.car_model}`
        );
      }, "");
    return cardata;
  }
  return null;
};
module.exports = carusingId;
